﻿using BnSFoVChanger.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Linq;

namespace BnSFoVChanger.ViewModel
{
    public class MainWindowViewModel : ViewModelBase
    {
        private int _defaultValue;
        private string _ZoomValue;
        private bool _isGameRunning;
        private bool _isGameClosed;

        private DelegateCommand _setDefaultValue;
        private DelegateCommand _applyValue;

        public MainWindowViewModel()
        {
            IsGameRunning = false;
            IsGameClosed = true;

            TestGridVisibleToggle = new DelegateCommand((o) => IsGameRunning = true);
        }

        public int DefaultValue { get => _defaultValue; set => SetField(ref _defaultValue, value); }
        public string ZoomValue { get => _ZoomValue; set => SetField(ref _ZoomValue, value); }
        public bool IsGameRunning { get => _isGameRunning; set => SetField(ref _isGameRunning, value); }
        public bool IsGameClosed { get => _isGameClosed; set => SetField(ref _isGameClosed, value); }
        public ICommand SetDefaultValue { get => _setDefaultValue ?? new DelegateCommand((o) => iSetDefaultValue()); }
        public ICommand ApplyValue { get => _applyValue ?? new DelegateCommand((o) => ReadConfiguration()); }
        public ICommand TestGridVisibleToggle { get; private set; }

        public void iSetDefaultValue()
        {
            int defaultvalue = 70;

            ZoomValue = defaultvalue.ToString();
        }

        public void ReadConfiguration()
        {
            //string configFile = @"C:\Users\" + Environment.UserName + @"\OneDrive\Documents\BnS\NCWEST\ClientConfiguration.xml";
            string configFile = @"C:\Users\alexa\OneDrive\Dokumente\BnS\NCWEST\ClientConfiguration.xml";

            XDocument xdoc = XDocument.Load(configFile);

            //var maxZoomValue = xdoc.Root.Elements("option").Select(x => x.Attribute("name").Value == "maxZoom").ToString();
        }

        public void ApplyFoVConfiguration()
        {
            string configFile = @"C:\Users\alexa\OneDrive\Dokumente\BnS\NCWEST\ClientConfiguration.xml";

            XDocument xdoc = XDocument.Load(configFile);

            var elem = xdoc.Elements("options").Select(x => x.Attribute("name").Value == "maxZoom");
        }

        public bool IsBnSRunning(string process)
        {
            return Process.GetProcessesByName(process).Length > 0;
        }
    }
}
